import React from 'react';
import './styles/admin.css';

class Admin extends React.Component{
    constructor(props){
        super(props);
        this._bindingAdd = this._bindingAdd.bind(this);
        this._bindingDel = this._bindingDel.bind(this);
        this._bindingEdit = this._bindingEdit.bind(this);
        this._bindingView = this._bindingView.bind(this);
    }

    _bindingAdd(){
        console.log("Добавить");
    }
    _bindingDel(){
        console.log("Удалить");
    }
    _bindingEdit(){
        console.log("Редактировать");
    }
    _bindingView(){
        console.log("Показать");
    }
    render(){
        return(
            <div className="p-4">
                <h3>Admin</h3>
                <div>
                    <table>
                        <thead><tr>
                            <th><button className="btn btn-outline-dark"
                            onClick={this._bindingAdd}>Добавить</button></th>
                            <th><button className="btn btn-outline-dark"
                            onClick={this._bindingDel}>Удалить</button></th>
                            <th><button className="btn btn-outline-dark"
                            onClick={this._bindingEdit}>Редактировать</button></th>
                            <th><button className="btn btn-outline-dark"
                            onClick={this._bindingView}>Показать</button></th>
                        </tr></thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        )
    }
}
export default Admin;