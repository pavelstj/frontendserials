import React from 'react';
import './styles/users.css';

const arrayArticle = [
    {
        id: 1,
        login: "pavelstj",
        firstName: "Скуратовец",
        middleName: "Павел",
        lastName: "Сергеевич",
        email: "pavelstj@gmail.com",
        phone: "375298903603",
        role: "ADMIN"
    },
    {
        id: 2,
        login: "kny",
        firstName: "Кузнецов",
        middleName: "Никита",
        lastName: "Юрьевич",
        email: "kny-155@gmail.com",
        phone: "375296661289",
        role: "USER"
    },
    {
        id: 3,
        login: "oleg",
        firstName: "Кот",
        middleName: "Олег",
        lastName: "Олегович",
        email: "olegforever@mail.ru",
        phone: "375331234567",
        role: "USER"
    }
];

class Users extends React.Component{
    constructor(props){
        super(props);
        this._buttonClicked = this._buttonClicked.bind(this);
        this.state = {
            data: '1',
            rows: 1
        }
    }

    _buttonClicked(){
        this.setState({
            rows: parseInt(this.state.data)
        });
     }

    render(){
        const userTable = arrayArticle.map(function (value, index) {
            return(
                <tr key={index}><td>{value.id}</td><td>{value.login}</td><td>{value.firstName}</td>
                    <td>{value.middleName}</td><td>{value.lastName}</td>
                    <td>{value.email}</td><td>{value.phone}</td><td>{value.role}</td>
                </tr>
            )
        });

        return(
            <div className="p-4">
                <div className="py-2 d-none d-md-inline-block px-3">
                    <form className="form-inline my-2 my-lg-0">
                        <label className=""><h5 className="users-search-label">Поиск пользователя</h5></label>
                        <input className="form-control mr-sm-2 ml-sm-2 search-line" type="text" placeholder="Логин"
                               aria-label="Логин"/>
                        <button className="btn btn-outline-success my-2 my-sm-0 search-line"
                                type="submit">Поиск
                        </button>
                    </form>
                </div>
                    <table className="table table-bordered">
                        <thead className="thead-light">
                        <tr><th>Id</th><th>Логин</th><th>Фамилия</th><th>Имя</th>
                            <th>Отчество</th><th>Email</th><th>Телефон</th><th>Роль</th></tr>
                        </thead>
                        <tbody>{userTable}</tbody>
                    </table>
            </div>
        )
    }
}
export default Users;