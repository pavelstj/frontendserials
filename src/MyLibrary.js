import React from 'react';
import Yui from './images/yui.png';
import List from './images/list.svg';
import Fire from './images/fire.svg';
import Radar from './images/radar.svg';
import './styles/mylibrary.css';

const infoYui = {
    name: 'Юи Хирасава',
    date: '1991-forever',
    status: 'Гитарист и вокалист группы Ho-Kago Tea Time',
    poster: Yui,
    overviewYui:  'Главный персонаж «K-On!». Является одним из членов\n' +
        'клуба лёгкой музыки и играет на электрогитаре Heritage\n' +
        'Cherry Sunburst Gibson Les Paul Standard. Не получает\n' +
        'хороших оценок в школе (хотя при достаточной подготовке\n' +
        'способна достигать высоких результатов, как было, когда\n' +
        'она готовилась к повторной сдаче теста и получила максимальный\n' +
        'балл после репетиторства Мио) и легко отвлекается по мелочам.\n' +
        'Обладает добродушным и лёгким характером, но при достаточном\n' +
        'настрое со стороны окружающих способна проявлять высокую концентрацию\n' +
        'внимания, ограниченную, тем не менее, лишь одним предметом за раз.\n' +
        'В процессе полностью игнорирует другие вещи, вплоть до забывания\n' +
        'всего, чему научилась ранее. Любит вкусно поесть. Её фамилия была\n' +
        'взята у гитариста P-Model Сусуму Хирасавы.'
};


class MyLibrary extends React.Component{
    render(){
        const epContain = <div className="episode-card row">
            <div className="col-3 nec-poster">
                <img className="" src={infoYui.poster}/>
            </div>
            <div className="col-8 container">
                <div className="my-auto ec-info">
                    <h4>{infoYui.name}</h4>
                    <div className="row mb-2 align-items-center">
                        <img className="min-icon col-1" src={Fire}/>
                        <span className="spanS col">Серия, на которой остановился</span>
                    </div>
                    <div className="row mb-2 align-items-center">
                        <img className="min-icon col-1" src={Radar}/>
                        <span className="spanS col">Последняя вышедшая серия</span>
                    </div>
                </div>
            </div>
            <div className="col-1 p-2">
                <img className="e-list-icon ml-4" src={List}/>
            </div>
        </div>;

        return(
                <div className="p-4">
                    <h3 className="mb-3">Моя библиотека</h3>
                    <div className="">
                        {epContain}
                        {epContain}
                        {epContain}
                        {epContain}
                    </div>
                </div>
        )
    }
}
export default MyLibrary;