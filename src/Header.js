import React from 'react';
import myLogoBlack from "./images/myLogoBlack.png";
import myLogo from "./images/myLogo.png";
import {Link} from "react-router-dom";
import male from "./images/male.svg";
import './styles/header.css';

const modal = <div className="modal fade" id="exampleModalCenter" tabIndex="-1" role="dialog"
                   aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content">
            <div className="modal-header">
                {/*<h5 className="modal-title" id="exampleModalLongTitle">Modal title</h5>*/}
                <img src={myLogoBlack} width="200" height="50" className="d-block mx-auto"/>
                {/*<button type="button" className="" data-dismiss="modal" aria-label="Close">*/}
                {/*<span aria-hidden="true">&times;</span>*/}
                {/*</button>*/}
            </div>
            <div className="modal-body">
                Модальное окно в графическом интерфейсе пользователя —
                окно, которое блокирует работу пользователя с родительским
                приложением до тех пор, пока пользователь это окно не закроет.
                Модальными преимущественно реализованы диалоговые окна. Также модальные
                окна часто используются для привлечения внимания пользователя к важному
                событию или критической ситуации.<br/>
                <a className="site-aS" href="https://ru.wikipedia.org/wiki/
                                %D0%9C%D0%BE%D0%B4%D0%B0%D0%BB%D1%8C%D0%BD%D0%BE%D0%B5_
                                %D0%BE%D0%BA%D0%BD%D0%BE">Wiki</a>
            </div>
            <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Отмена</button>
                <button type="button" className="btn btn-primary" data-dismiss="modal">Ок</button>
            </div>
        </div>
    </div>
</div>;

class Header extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
                <nav className="site-header sticky-top py-1">
                    <div className="container d-flex flex-column flex-md-row justify-content-between">
                        <a href="/">
                            <img src={myLogo} width="160" height="40" className="d-block mx-auto"/>
                        </a>
                        <div className="py-2 d-none d-md-inline-block">
                            <Link className="center" to="/library">Библиотека</Link>
                        </div>
                        <div className="py-2 d-none d-md-inline-block">
                            <Link className="center" to="/mylibrary">Моя библиотека</Link>
                        </div>
                        <div className="py-2 d-none d-md-inline-block">
                            <Link className="center" to="/mytiming">Мое расписание</Link>
                        </div>
                        <div className="py-2 d-none d-md-inline-block">
                            <form className="form-inline my-2 my-lg-0">
                                <input className="form-control mr-sm-2 search-line" type="text" placeholder="Поиск"
                                       aria-label="Поиск"/>
                                <button className="btn btn-outline-success my-2 my-sm-0 search-line"
                                        type="submit">Поиск
                                </button>
                            </form>
                        </div>
                        <div className="py-2 d-none d-md-inline-block">
                            <div className="dropdown">
                                <a className="dropdown-toggle test-button"
                                   id="dropdownMenuButton" data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">
                                    <img src={male} width="30" height="30"/>
                                </a>
                                <div className="dropdown-menu mt-2" aria-labelledby="dropdownMenuButton">
                                    <a className="dropdown-item" href="#">Вход</a>
                                    <a className="dropdown-item" href="#">Регистрация</a>
                                    <a className="dropdown-item" href="#">Настройки</a>
                                    <Link to="/users" className="dropdown-item">Пользователи</Link>
                                    <Link to="/admin" className="dropdown-item">Админка</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
        )
    }
}
export default Header;