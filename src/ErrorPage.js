import React from 'react';
import {Link} from 'react-router-dom';
class ErrorPage extends React.Component{
    render(){
        return(
            <div className="p-4">
                <div className="alert alert-danger">
                    <h2>Страница не найдена</h2>
                    <p><a className="site-aS" href="/">На главную</a></p>
                </div>
            </div>
        )
    }
}
export default ErrorPage;