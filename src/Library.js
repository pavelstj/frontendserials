import React from 'react';
import './styles/index.css';
import './styles/filmcard.css';
import Axios from 'axios';
import './styles/library.css';
import plus from './images/plus.svg';
import {Link} from "react-router-dom";

const baseURL = 'https://api.themoviedb.org/3/';
const api_key = 'b6e8c3a8e128bbff9b1e30d402270a96';
const imgURL = 'https://image.tmdb.org/t/p/w200';

class Library extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            getPopularTv: []
        }
    }

    componentDidMount(){
        Axios.get(baseURL+'tv/popular?page=1&language=ru&api_key='+api_key).then(response => {
            this.setState({getPopularTv: response.data.results});
        });
    }

    render(){

        const filmCard = this.state.getPopularTv.map((film, index)=><div key={index} className="item-poster-cardS">
            <div className="image-contentS">
                <Link to="/serial">
                    <img className="poster-imageS" src={imgURL+film.poster_path} sizes="185px"/>
                </Link>
            </div>
            <div className="infoS">
                <div className="wrapperS">
                    <div className="flexS">
                        <Link to="/serial"><span className="title-resourceS">{film.name}</span></Link>
                        <span className="spanS">(1000-3000)-100% инфа</span>
                    </div>
                </div>
                <p className="overviewS">
                    <ul>
                        <li><span>Id: </span>{film.id}</li>
                        <li><span>Сезонов: </span>несколько</li>
                        <li><span>Статус: </span>выходит</li>
                        <li><span>Жанр: </span>комедия</li>
                    </ul>
                </p>
                <div className="view-moreS">
                    <span className="kS">Подробнее</span>
                    <div className="card-icon ml-auto">
                        <img src={plus}/>
                    </div>
                </div>
            </div>
        </div>);

        return(
            <div className="p-4">
                <div className="container tv-menu-con">
                    <div className="row">
                        <div className="input-group col">
                            <div className="input-group-prepend">
                                <label className="input-group-text" htmlFor="inputGroupSelect01">Жанр</label>
                            </div>
                            <select className="custom-select" id="inputGroupSelect01">
                                <option selected>Не учитывать</option>
                                <option value="1">Драма</option>
                                <option value="2">Комедия</option>
                                <option value="3">Ужасы</option>
                                <option value="4">Фантастика</option>
                                <option value="5">Боевик</option>
                                <option value="6">Приключения</option>
                            </select>
                        </div>
                        <div className="input-group col">
                            <div className="input-group-prepend">
                                <label className="input-group-text" htmlFor="inputGroupSelect01">Статус</label>
                            </div>
                            <select className="custom-select" id="inputGroupSelect01">
                                <option selected>Не учитывать</option>
                                <option value="1">Вышел</option>
                                <option value="2">Онгоинг</option>
                                <option value="3">Анонс</option>
                            </select>
                        </div>
                        <div className="input-group col">
                            <div className="input-group-prepend">
                                <label className="input-group-text" htmlFor="inputGroupSelect01">Год</label>
                            </div>
                            <select className="custom-select" id="inputGroupSelect01">
                                <option selected>Не учитывать</option>
                                <option value="1">2018</option><option value="2">2017</option>
                                <option value="3">2016</option><option value="4">2015</option>
                                <option value="5">2014</option><option value="6">2013</option>
                                <option value="7">2012</option><option value="8">2011</option>
                                <option value="9">2005-2010</option><option value="10">2000-2005</option>
                                <option value="11">1990-2000</option><option value="12">1980-1990</option>
                                <option value="13">до 1980</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div className="all-film-card">
                    {filmCard}
                </div>
            </div>
        )
    }
}
export default Library;