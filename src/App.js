import React from "react";
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Home from './Home';
import ErrorPage from './ErrorPage';
import Library from './Library';
import MyLibrary from './MyLibrary';
import Timing from './Timing';
import Header from './Header';
import Serial from './Serial';
import Users from './Users';
import Admin from './Admin';

const SerialC = () => (<Serial/>);
const MyLibraryC = () => (<MyLibrary/>);
const TimingC = () => (<Timing/>);
const HomeC = () => (<Home/>);
const LibraryC = () => (<Library/>);

class App extends React.Component{
    render(){
        return(
            <div>
                <BrowserRouter>
                    <div>
                        <Header/>
                        <div className="mediaS">
                            <Switch>
                                <Route exact path="/" component={HomeC}/>
                                <Route path="/serial" component={SerialC}/>
                                <Route exact path="/mytiming" component={TimingC}/>
                                <Route path="/library" component={LibraryC}/>
                                <Route path="/mylibrary" component={MyLibraryC}/>
                                <Route exact path="/users" component={Users}/>
                                <Route exact path="/admin" component={Admin}/>
                                <Route path="*" component={ErrorPage}/>
                            </Switch>
                        </div>
                    </div>
                </BrowserRouter>
            </div>
        )
    }
}
export default App;