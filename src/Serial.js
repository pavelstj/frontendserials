import React from 'react';
import './styles/serial.css';
import Yui from './images/yui.png';
import Share from './images/share.svg';
import Checked from './images/checked.svg';
import Axios from 'axios';

const infoYui = {
  name: 'Юи Хирасава',
  date: '1991-forever',
  status: 'Гитарист и вокалист группы Ho-Kago Tea Time',
  poster: Yui,
  overviewYui:  'Главный персонаж «K-On!». Является одним из членов\n' +
      'клуба лёгкой музыки и играет на электрогитаре Heritage\n' +
      'Cherry Sunburst Gibson Les Paul Standard. Не получает\n' +
      'хороших оценок в школе (хотя при достаточной подготовке\n' +
      'способна достигать высоких результатов, как было, когда\n' +
      'она готовилась к повторной сдаче теста и получила максимальный\n' +
      'балл после репетиторства Мио) и легко отвлекается по мелочам.\n' +
      'Обладает добродушным и лёгким характером, но при достаточном\n' +
      'настрое со стороны окружающих способна проявлять высокую концентрацию\n' +
      'внимания, ограниченную, тем не менее, лишь одним предметом за раз.\n' +
      'В процессе полностью игнорирует другие вещи, вплоть до забывания\n' +
      'всего, чему научилась ранее. Любит вкусно поесть. Её фамилия была\n' +
      'взята у гитариста P-Model Сусуму Хирасавы.'
};

const baseURL = 'https://api.themoviedb.org/3/';
const api_key = 'b6e8c3a8e128bbff9b1e30d402270a96';
const imgURL = 'https://image.tmdb.org/t/p/w200';
const tv_id = '1622';

class Serial extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            last_episode_to_air: {},
            next_episode_to_air: {},
            n_ep_still_path: '',
            first_date: '',
            last_date: '',
            genres: '',
            serial: {}
        }
    }

    componentDidMount(){
        let text = '';
        Axios.get(baseURL+'tv/'+tv_id+'?language=ru&api_key='+api_key).then(response => {
            for (let i = 0; i < response.data.genres.length; i++) {
                if (i === 0) text += response.data.genres[i].name;
                else text += ', '+response.data.genres[i].name;
            }
            this.setState({serial: response.data,
                genres: text,
                last_episode_to_air: response.data.last_episode_to_air,
                next_episode_to_air: response.data.next_episode_to_air,
            first_date: response.data.first_air_date.substr(0,4),
            last_date: response.data.last_air_date.substr(0,4)});
            if (response.data.next_episode_to_air.still_path === null) {
                this.setState({n_ep_still_path: response.data.poster_path});
            } else this.setState({n_ep_still_path: response.data.next_episode_to_air.still_path})
        });
    }

    render(){
        return(
            <div>
                <div className="serial-header container">
                    <div className="row">
                        <div className="col-3">
                            <img className="sh-img d-flex mx-auto" src={imgURL+this.state.serial.poster_path}/>
                        </div>
                        <div className="col-9">
                            <div className="serial-header-info mb-2 row">
                                <div className="col">
                                    <h4>{this.state.serial.name}<br/><span className="spanS">
                                        ({this.state.first_date}-{this.state.last_date})
                                    </span></h4>
                                    <div className="serial-genre">{this.state.genres}</div>
                                </div>
                                <div className="col">
                                    <div className="row">
                                        <div className="flex-column d-flex mx-auto">
                                            <img className="share-img mx-auto" src={Share}/>
                                            <div className="share-info">Просмотрено серий</div>
                                        </div>
                                        <div className="flex-column d-flex mx-auto">
                                            <img className="share-img mx-auto" src={Share}/>
                                            <div className="share-info">Осталось серий</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="serial-header-overview">
                                <p className="overviewS">
                                    {this.state.serial.overview}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="serial-body p-4">
                    <div className="flex-column mb-5">
                            <h4>Следующая серия</h4>
                        <div className="episode-card row">
                            <div className="col-3">
                                <img className="ec-poster" src={imgURL+this.state.last_episode_to_air.still_path}/>
                            </div>
                            <div className="col-8 d-flex">
                                <div className="my-auto ec-info ">
                                    <h4>{this.state.last_episode_to_air.name}</h4>
                                    <h6>Сезон: <span className="spanS">
                                        {this.state.last_episode_to_air.season_number}
                                    </span></h6>
                                    <h6>Вышла: <span className="spanS">
                                        {this.state.last_episode_to_air.air_date}
                                    </span></h6>
                                </div>
                            </div>
                            <div className="ec-img col-1 p-2">
                                <img className="" src={Checked}/>
                            </div>
                        </div>
                    </div>
                    <div className="flex-column">
                        <h4>Скоро</h4>
                        <div className="episode-card row">
                            <div className="col-3 nec-poster">
                                <img className="" src={imgURL+this.state.n_ep_still_path}/>
                            </div>
                            <div className="col-9 d-flex">
                                <div className="my-auto ec-info ">
                                    <h4>{this.state.next_episode_to_air.name}</h4>
                                    <h6>Сезон: <span className="spanS">
                                        {this.state.next_episode_to_air.season_number}
                                    </span></h6>
                                    <h6>Выйдет: <span className="spanS">
                                        {this.state.next_episode_to_air.air_date}
                                    </span></h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Serial;